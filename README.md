# Rusty Music Player

[![](https://img.shields.io/crates/v/rmup)](https://crates.io/crates/rmup) [![](https://img.shields.io/aur/version/rmup)](https://aur.archlinux.org/packages/rmup)

## About

RMuP is a cross-platform TUI music player written in rust.

## Installation

### Cargo

Run the following command to install RMuP using cargo

```sh
cargo install rmup
```

### Arch Linux

On Arch Linux, RMuP can be installed from the [AUR](https://aur.archlinux.org/packages/rmup).

## License

RMuP is free software: you can redistribute it and/or modify it under the terms of the Mozilla Public License, version 2.0. See the LICENSE file or go to https://mozilla.org/MPL/2.0/ for license terms and conditions.
