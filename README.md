# Rusty Music Player

## About

RMuP is a TUI music player written in rust. It is intended to be cross-platform, but has currently only been tested on Linux.

## Roadmap

The following is the current list of to-dos in no particular order:

- [ ] Finish interface functionality.
    - [ ] Add the ability to queue up and play an entire artist/album/playlist by pressing enter while it's selected.
    - [ ] Submenu for albums by a given artist.
    - [ ] Track progress bar.
- [ ] Add playlist functionality.
- [ ] Implement shuffle and repeat.
- [x] Refactor player.rs so that it's not all one enormous function.
- [-] Add support for OGG and OPUS.
    - [x] OGG
    - [ ] OPUS

## License

RMuP is free software: you can redistribute it and/or modify it under the terms of the Mozilla Public License, version 2.0. See the LICENSE file or go to https://mozilla.org/MPL/2.0/ for license terms and conditions.