/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use ratatui::{
    layout::{Constraint, Direction, Layout},
    style::Style,
    text::Text,
    widgets::{Block, Borders, Paragraph},
};

use crate::{library::track::Track, media_system::Queueable};

use super::{MovementDirection, Screen, UIList};

pub struct QueueScreen<'a> {
    now_playing: Option<Track>,
    queue: UIList<'a, Track>,
    normal_style: Style,
}

impl<'a> QueueScreen<'a> {
    pub fn new(list: &'a [Track], normal_style: &Style) -> Self {
        let mut queue = UIList::new(list, "Up Next", normal_style);
        queue.select(0);

        Self {
            now_playing: None,
            queue,
            normal_style: *normal_style,
        }
    }

    pub fn update_now_playing(&mut self, track: &Option<Track>) {
        track.clone_into(&mut self.now_playing);
    }

    pub fn update_queue(&mut self, queue: Vec<Track>) {
        self.queue.update(queue);
        self.queue.select(0);
    }
}

impl Screen for QueueScreen<'_> {
    fn ui(&self, f: &mut ratatui::Frame, page_chunk: ratatui::prelude::Rect) {
        // Split the screen into top and bottom halves
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(7), Constraint::Fill(1)].as_ref())
            .split(page_chunk);

        let (title, album, number, artist, year) = self.now_playing.as_ref().map_or_else(
            || {
                (
                    "N/A",
                    "N/A".into(),
                    "N/A".into(),
                    "N/A".into(),
                    "N/A".into(),
                )
            },
            |track| {
                (
                    track.title.as_ref().map_or("N/A", |title| title),
                    track.album.clone(),
                    track
                        .number
                        .map_or_else(|| "N/A".into(), |number| number.to_string()),
                    track.artist.clone(),
                    track
                        .year
                        .map_or_else(|| "N/A".into(), |year| year.to_string()),
                )
            },
        );

        let now_playing_text = Text::from(format!(
            "Title        \u{2502} {title}\n\
             Album        \u{2502} {album}\n\
             Track Number \u{2502} {number}\n\
             Artist       \u{2502} {artist}\n\
             Year         \u{2502} {year}"
        ));

        let now_playing_para = Paragraph::new(now_playing_text)
            .block(Block::default().title("Now Playing").borders(Borders::ALL))
            .style(self.normal_style);

        f.render_widget(now_playing_para, chunks[0]);

        // Render track list in bottom
        let mut queue_state = self.queue.state.clone();
        f.render_stateful_widget(self.queue.display.clone(), chunks[1], &mut queue_state);
    }

    fn style_panels(
        &mut self,
        selected: &ratatui::prelude::Style,
        _unselected: &ratatui::prelude::Style,
    ) {
        self.queue.set_highlight_style(*selected);
    }

    fn switch_panel(&mut self, _direction: super::MovementDirection) {}

    fn switch_item(&mut self, direction: super::MovementDirection) {
        let mut selected = self.queue.selected();
        if self.queue.len() > 0 {
            match direction {
                MovementDirection::Prev => {
                    if selected == 0 {
                        selected = self.queue.len() - 1;
                    } else {
                        selected -= 1;
                    }
                }
                MovementDirection::Next => {
                    if selected == self.queue.len() - 1 {
                        selected = 0;
                    } else {
                        selected += 1;
                    }
                }
                MovementDirection::Top => selected = 0,
                MovementDirection::Bottom => selected = self.queue.len() - 1,
            }
            self.queue.select(selected);
        }
    }

    fn update_lists(&mut self, _normal_style: &ratatui::prelude::Style) {}

    fn get_selected(&self, _tracks_current_only: bool) -> Queueable {
        let index = self.queue.selected();
        let mut v = self.queue.list()[index..].to_vec();
        v.append(&mut self.queue.list()[..index].to_vec());
        Queueable::TrackList(v.into())
    }
}
