/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

 use std::{
    collections::VecDeque,
    io::{self, Stdout},
    process
};

use anyhow::Result;
use crossterm::{
    event::{
        self, Event, KeyCode, KeyEvent
    },
    execute,
    terminal::{
        disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen
    }
};

use tui::{
    backend::CrosstermBackend,
    Terminal,
    layout::{
        Layout, Constraint, Direction
    },
    style::{
        Color,
        Style
    },
    widgets::{
        Block, Borders, List, ListItem, ListState
    }
};

use crate::audio::AudioSystem;
use crate::music_library::{Library, Track, Album, Artist, Playlist};

#[derive(PartialEq)]
enum Panel {
    Left,
    Right
}

#[derive(PartialEq)]
enum View {
    Artist,
    Album,
    Playlists
}

struct PlayerState<'a> {

    audio_sys: AudioSystem,

    // Queue of tracks to be played
    play_queue: VecDeque<Track>,

    // Style for unhighlighted items
    normal_style: Style,

    // Style for highlighted item in the currently selected pane
    highlight1_style: Style,

    // Style for highlighted item in the unselected pane
    highlight2_style: Style,

    // List of Artist structs
    artist_list: Vec<Artist>,

    // List of Artist names each encapsulated by a ListItem for tui display
    artist_name_list: Vec<ListItem<'a>>,

    // List of Album structs
    album_list: Vec<Album>,

    // List of Album names each encapsulated by a ListItem for tui display
    album_name_list: Vec<ListItem<'a>>,

    // List of Playlist structs
    playlist_list: Vec<Playlist>,

    // List of Playlist names each encapsulated by a ListItem for tui display
    playlist_name_list: Vec<ListItem<'a>>,

    // Artist list as a tui List with highlight 1
    artists_display_1: List<'a>,

    // Artist list as a tui List with highlight 2
    artists_display_2: List<'a>,

    // ListState for the artist List
    artist_list_state: ListState,

    // Album list as a tui List with highlight 1
    albums_display_1: List<'a>,

    // Album list as a tui List with highlight 2
    albums_display_2: List<'a>,

    // ListState for the album list
    album_list_state: ListState,

    // Playlist list as a tui List with highlight 1
    playlists_display_1: List<'a>,

    // Playlist list as a tui List with highlight 2
    playlists_display_2: List<'a>,

    // ListState for playlist list
    playlist_list_state: ListState,

    // tui List that displays the contents of the artist/album/playlist selected in the left
    right_pane: List<'a>,

    // list of the items in the right pane
    right_pane_items: Vec<ListItem<'a>>,

    // ListState for the right pane
    right_pane_items_state: ListState,

    // A reference to whichever List widget should currently be in view
    current_view: List<'a>,

    // A reference to the ListState corresponding to the list currently in view
    current_view_state: ListState,

    // Which panel (left or right) is currently selected
    current_panel: Panel,

    // Which view (artist/album/playlist) we are currently in
    current_view_type: View

}

impl <'a> PlayerState<'a> {
    pub fn try_new(library: &Library) -> Result<Self> {
        let artist_list = library.artists().clone();
        let artist_name_list = {
            let mut v = Vec::new();
            for a in artist_list.iter() {
                v.push(ListItem::new(a.name().clone()));
            }
            v
        };

        let album_list = library.albums().clone();
        let album_name_list = {
            let mut v = Vec::new();
            for a in album_list.iter() {
                v.push(ListItem::new(a.name().clone()));
            }
            v
        };

        let playlist_list = library.playlists().clone();
        let playlist_name_list = {
            let mut v = Vec::new();
            for a in playlist_list.iter() {
                v.push(ListItem::new(a.name().clone()));
            }
            v
        };

        let normal_style = Style::default();
        let highlight1_style = Style::default()
            .fg(Color::Black)
            .bg(Color::White);
        let highlight2_style = Style::default()
            .fg(Color::Red)
            .bg(Color::Reset);

        let artists_display_1 = List::new(artist_name_list.clone())
            .block(Block::default()
                .title(" Artists ")
                .borders(Borders::ALL)
            )
            .style(normal_style)
            .highlight_style(highlight1_style);

        let artists_display_2 = List::new(artist_name_list.clone())
            .block(Block::default()
                .title(" Artists ")
                .borders(Borders::ALL)
            )
            .style(normal_style)
            .highlight_style(highlight2_style);
        
        let albums_display_1 = List::new(album_name_list.clone())
            .block(Block::default()
                .title(" Albums ")
                .borders(Borders::ALL)
            )
            .style(normal_style)
            .highlight_style(highlight1_style);

        let albums_display_2 = List::new(album_name_list.clone())
            .block(Block::default()
                .title(" Albums ")
                .borders(Borders::ALL)
            )
            .style(normal_style)
            .highlight_style(highlight2_style);

        let playlists_display_1 = List::new(playlist_name_list.clone())
            .block(Block::default()
                .title(" Playlists ")
                .borders(Borders::ALL)
            )
            .style(normal_style)
            .highlight_style(highlight1_style);

        let playlists_display_2 = List::new(playlist_name_list.clone())
            .block(Block::default()
                .title(" Playlists ")
                .borders(Borders::ALL)
            )
            .style(normal_style)
            .highlight_style(highlight2_style);
        
        let mut artist_list_state = ListState::default();
        let mut album_list_state = ListState::default();
        let mut playlist_list_state = ListState::default();
        artist_list_state.select(Some(0));
        album_list_state.select(Some(0));
        playlist_list_state.select(Some(0));

        let right_pane_items = if let Some(first_artist) = artist_list.get(0) {
            let albs = first_artist.albums();
            let mut v = Vec::new();
            for a in albs.iter() {
                v.push(ListItem::new(a.name().clone()))
            }
            v
        } else {
            Vec::new()
        };

        let right_pane = List::new(right_pane_items.clone())
            .block(Block::default().borders(Borders::ALL))
            .style(normal_style)
            .highlight_style(highlight1_style);

        let mut right_pane_items_state = ListState::default();
        right_pane_items_state.select(None);

        let current_view = artists_display_1.clone();
        let current_view_state = artist_list_state.clone();

        Ok(PlayerState {
            audio_sys: AudioSystem::try_new()?,
            play_queue: VecDeque::new(),
            normal_style,
            highlight1_style,
            highlight2_style,
            artist_list,
            artist_name_list,
            album_list,
            album_name_list,
            playlist_list,
            playlist_name_list,
            artists_display_1,
            artists_display_2,
            artist_list_state,
            albums_display_1,
            albums_display_2,
            album_list_state,
            playlists_display_1,
            playlists_display_2,
            playlist_list_state,
            right_pane,
            right_pane_items,
            right_pane_items_state,
            current_view,
            current_view_state,
            current_panel: Panel::Left,
            current_view_type: View::Artist
        })
    }

    
}

fn do_key_command<'a>(state: &'a mut PlayerState, library: &Library, terminal: &mut Terminal<CrosstermBackend<Stdout>>, ke: KeyEvent) -> Result<()> {
    match ke.code {
        // Switch to artists view
        KeyCode::Char('1') => {
            state.current_view = state.artists_display_1.clone();
            state.current_view_state = state.artist_list_state.clone();
            state.right_pane_items = if let Some(sel_artist) = state.artist_list.get(state.current_view_state.selected().unwrap_or(0)) {
                let a = sel_artist.albums();
                let mut v = Vec::new();
                for alb in a.iter() {
                    v.push(ListItem::new(alb.name().clone()));
                }
                v
            } else {
                Vec::new()
            };
            state.right_pane = List::new(state.right_pane_items.clone())
                .block(Block::default().borders(Borders::ALL))
                .style(state.normal_style)
                .highlight_style(state.highlight1_style);
            state.current_view_type = View::Artist;
            state.current_panel = Panel::Left;
            state.right_pane_items_state.select(None);
        },

        // Switch to albums view
        KeyCode::Char('2') => {
            state.current_view = state.albums_display_1.clone();
            state.current_view_state = state.album_list_state.clone();
            state.right_pane_items = if let Some(sel_album) = state.album_list.get(state.current_view_state.selected().unwrap_or(0)) {
                let t = sel_album.tracks();
                let mut v = Vec::new();
                for track in t.iter() {
                    v.push(ListItem::new(track.metadata().get("Title").unwrap_or(&track.path().to_str().unwrap().to_string()).clone()));
                }
                v
            } else {
                Vec::new()
            };
            state.right_pane = List::new(state.right_pane_items.clone())
                .block(Block::default().borders(Borders::ALL))
                .style(state.normal_style)
                .highlight_style(state.highlight1_style);
            state.current_view_type = View::Album;
            state.current_panel = Panel::Left;
            state.right_pane_items_state.select(None);
        },

        // Switch to playlists view
        KeyCode::Char('3') => {
            state.current_view = state.playlists_display_1.clone();
            state.current_view_state = state.playlist_list_state.clone();
            state.right_pane_items = if let Some(sel_playlist) = state.playlist_list.get(state.current_view_state.selected().unwrap_or(0)) {
                let t = sel_playlist.tracks();
                let mut v = Vec::new();
                for track in t.iter() {
                    v.push(ListItem::new(track.metadata().get("Title").unwrap_or(&track.path().to_str().unwrap().to_string()).clone()));
                }
                v
            } else {
                Vec::new()
            };
            state.right_pane = List::new(state.right_pane_items.clone())
                .block(Block::default().borders(Borders::ALL))
                .style(state.normal_style)
                .highlight_style(state.highlight1_style);
            state.current_view_type = View::Playlists;
            state.current_panel = Panel::Left;
            state.right_pane_items_state.select(None);
        },

        // Quit
        KeyCode::Char('q') | KeyCode::Char('Q') => {
            disable_raw_mode();
            execute!(io::stdout(), LeaveAlternateScreen)?;
            terminal.show_cursor()?;
            process::exit(0);
        },

        // Move down in the currently selected list
        KeyCode::Char('j') => {
            if state.current_panel == Panel::Left {
                let selected = state.current_view_state.selected().unwrap_or(0);

                // find the number of items in the list so we don't go past the end
                let list_max = match state.current_view_type {
                    View::Artist =>  if state.artist_list.len() > 0 {
                        state.artist_list.len() - 1
                    } else {
                        0
                    },
                    View::Album => if state.album_list.len() > 0 {
                        state.album_list.len() - 1
                    } else {
                        0
                    },
                    View::Playlists => if state.playlist_list.len() > 0 {
                        state.playlist_list.len() - 1
                    } else {
                        0
                    }
                };

                // change the view self to highlight the next or same item
                state.current_view_state.select(Some(
                    if selected == list_max {
                        list_max
                    } else {
                        selected + 1
                    }
                ));

                // Change right pane to show contents of currently selected item
                match state.current_view_type {
                    View::Artist => {
                        state.right_pane_items = if let Some(sel_artist) = state.artist_list.get(state.current_view_state.selected().unwrap_or(0)) {
                            let a = sel_artist.albums();
                            let mut v = Vec::new();
                            for alb in a.iter() {
                                v.push(ListItem::new(alb.name().clone()));
                            }
                            v
                        } else {
                            Vec::new()
                        };
                        state.right_pane = List::new(state.right_pane_items.clone())
                            .block(Block::default().borders(Borders::ALL))
                            .style(state.normal_style)
                            .highlight_style(state.highlight1_style);
                    },
                    View::Album => {
                        state.right_pane_items = if let Some(sel_album) = state.album_list.get(state.current_view_state.selected().unwrap_or(0)) {
                            let t = sel_album.tracks();
                            let mut v = Vec::new();
                            for track in t.iter() {
                                v.push(ListItem::new(track.metadata().get("Title").unwrap_or(&track.path().to_str().unwrap().to_string()).clone()));
                            }
                            v
                        } else {
                            Vec::new()
                        };
                        state.right_pane = List::new(state.right_pane_items.clone())
                            .block(Block::default().borders(Borders::ALL))
                            .style(state.normal_style)
                            .highlight_style(state.highlight1_style);
                    },
                    View::Playlists => {
                        state.right_pane_items = if let Some(sel_playlist) = state.playlist_list.get(state.current_view_state.selected().unwrap_or(0)) {
                            let t = sel_playlist.tracks();
                            let mut v = Vec::new();
                            for track in t.iter() {
                                v.push(ListItem::new(track.metadata().get("Title").unwrap_or(&track.path().to_str().unwrap().to_string()).clone()));
                            }
                            v
                        } else {
                            Vec::new()
                        };
                        state.right_pane = List::new(state.right_pane_items.clone())
                            .block(Block::default().borders(Borders::ALL))
                            .style(state.normal_style)
                            .highlight_style(state.highlight1_style);
                    }
                }
            } else if state.current_panel == Panel::Right {
                let selected = state.right_pane_items_state.selected().unwrap_or(0);
                let list_max = state.right_pane_items.len() - 1;
                state.right_pane_items_state.select(Some(
                    if selected == list_max {
                        list_max
                    } else {
                        selected + 1
                    }
                ));
            }
        },

        // Move up in the currently selected list
        KeyCode::Char('k') => {
            if state.current_panel == Panel::Left {
                let selected = state.current_view_state.selected().unwrap_or(0);
                
                // change the view self to highlight the previous or same item
                state.current_view_state.select(Some(
                    if selected == 0 {
                        0
                    } else {
                        selected - 1
                    }
                ));

                // Change right pane to show contents of currently selected item
                match state.current_view_type {
                    View::Artist => {
                        state.right_pane_items = if let Some(sel_artist) = state.artist_list.get(state.current_view_state.selected().unwrap_or(0)) {
                            let a = sel_artist.albums();
                            let mut v = Vec::new();
                            for alb in a.iter() {
                                v.push(ListItem::new(alb.name().clone()));
                            }
                            v
                        } else {
                            Vec::new()
                        };
                        state.right_pane = List::new(state.right_pane_items.clone())
                            .block(Block::default().borders(Borders::ALL))
                            .style(state.normal_style)
                            .highlight_style(state.highlight1_style);
                    },
                    View::Album => {
                        state.right_pane_items = if let Some(sel_album) = state.album_list.get(state.current_view_state.selected().unwrap_or(0)) {
                            let t = sel_album.tracks();
                            let mut v = Vec::new();
                            for track in t.iter() {
                                v.push(ListItem::new(track.metadata().get("Title").unwrap_or(&track.path().to_str().unwrap().to_string()).clone()));
                            }
                            v
                        } else {
                            Vec::new()
                        };
                        state.right_pane = List::new(state.right_pane_items.clone())
                            .block(Block::default().borders(Borders::ALL))
                            .style(state.normal_style)
                            .highlight_style(state.highlight1_style);
                    },
                    View::Playlists => {
                        state.right_pane_items = if let Some(sel_playlist) = state.playlist_list.get(state.current_view_state.selected().unwrap_or(0)) {
                            let t = sel_playlist.tracks();
                            let mut v = Vec::new();
                            for track in t.iter() {
                                v.push(ListItem::new(track.metadata().get("Title").unwrap_or(&track.path().to_str().unwrap().to_string()).clone()));
                            }
                            v
                        } else {
                            Vec::new()
                        };
                        state.right_pane = List::new(state.right_pane_items.clone())
                            .block(Block::default().borders(Borders::ALL))
                            .style(state.normal_style)
                            .highlight_style(state.highlight1_style);
                    }
                }
            } else if state.current_panel == Panel::Right {
                let selected = state.right_pane_items_state.selected().unwrap_or(0);
                
                state.right_pane_items_state.select(Some(
                    if selected == 0 {
                        0
                    } else {
                        selected - 1
                    }
                ));
            }
        },

        // Move between the left and right panes
        KeyCode::Tab => {
            if state.current_panel == Panel::Left {
                state.current_panel = Panel::Right;
                state.current_view = match state.current_view_type {
                    View::Artist => state.artists_display_2.clone(),
                    View::Album => state.albums_display_2.clone(),
                    View::Playlists => state.playlists_display_2.clone() 
                };
                state.right_pane_items_state.select(Some(state.right_pane_items_state.selected().unwrap_or(0)));
            } else if state.current_panel == Panel::Right {
                state.current_panel = Panel::Left;
                state.current_view = match state.current_view_type {
                    View::Artist => state.artists_display_1.clone(),
                    View::Album => state.albums_display_1.clone(),
                    View::Playlists => state.playlists_display_1.clone() 
                };
                state.right_pane_items_state.select(None);
            }
        },

        // Start the highlighted track, or a track from the highlighted
        // album or artist, depending on the context
        KeyCode::Enter => {
            if state.current_view_type == View::Album && state.current_panel == Panel::Right {
                if let Some(track_n) = state.right_pane_items_state.selected() {
                    if let Some(album_n) = state.current_view_state.selected() {
                        let album = library.albums()[album_n].clone();
                        let track = album.tracks()[track_n].clone();
                        state.audio_sys.play_track(&track)?;
                    }
                }
            }
        },

        // Play/Pause
        KeyCode::Char('p') | KeyCode::Char('P') => {
            if state.audio_sys.is_paused() {
                state.audio_sys.resume();
            } else {
                state.audio_sys.pause();
            }
        }
        _ => ()
    };
    Ok(())
}

pub fn run(library: &Library) -> Result<()> {
    // Set up terminal
    let mut stdout = io::stdout();
    enable_raw_mode();
    execute!(stdout, EnterAlternateScreen)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let mut player_state = PlayerState::try_new(library)?;

    loop {
        terminal.clear()?;
        terminal.draw(|f| {
            let layout = Layout::default()
                .direction(Direction::Horizontal)
                .constraints(
                    [
                        Constraint::Percentage(50),
                        Constraint::Percentage(50)
                    ].as_ref()
                )
                .split(f.size());
            f.render_stateful_widget(player_state.current_view.clone(), layout[0], &mut player_state.current_view_state);
            f.render_stateful_widget(player_state.right_pane.clone(), layout[1], &mut player_state.right_pane_items_state);
        })?;
        if let Ok(Event::Key(ke)) = event::read() {
            do_key_command(&mut player_state, library, &mut terminal, ke)?;
        }
    }
}