/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{
    collections::HashMap,
    cmp::Ordering,
    fmt::Display,
    fs::{
        self, File
    },
    io::{
        self,
        ErrorKind
    },
    path::Path
};

use anyhow::Result;

use serde:: {
    Deserialize, Serialize
};

use crate::metadata;

#[derive(Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Track {
    file: String,
    metadata: HashMap<String, String>
}

#[derive(Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Album {
    name: String,
    tracks: Vec<Track>
}

#[derive(Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Artist {
    name: String,
    albums: Vec<Album>
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Playlist {
    name: String,
    tracks: Vec<Track>
}

#[derive(Clone, Serialize, Deserialize)]
pub struct LibraryData {
    albums: Vec<Album>,
    artists: Vec<Artist>,
    tracks: Vec<Track>,
    playlists: Vec<Playlist>
}

pub struct Library<'a> {
    db_path: &'a Path,
    data: LibraryData
}

impl Track {
    pub fn metadata(&self) -> &HashMap<String, String> {
        &self.metadata
    }

    pub fn metadata_mut(&mut self) -> &mut HashMap<String, String> {
        &mut self.metadata
    }

    pub fn path(&self) -> &Path {
        Path::new(&self.file)
    }
}

impl PartialOrd for Track {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.metadata().get("Album") == other.metadata().get("Album") { 
            if let Some(self_track) = self.metadata().get("Track") {
                let self_track_n = u32::from_str_radix(self_track, 10).expect(format!("Error: {} is not a number", self_track).as_str());
                if let Some(other_track) = other.metadata().get("Track") {
                    let other_track_n = u32::from_str_radix(other_track, 10).expect(format!("Error: {} is not a number", other_track).as_str());
                    return Some(self_track_n.cmp(&other_track_n));
                }
            }
        }
        None
    }
}

// Tracks sort by track number if they are in the same album. Otherwise, they
// sort by title (or filename if title is unavailable).
impl Ord for Track {
    fn cmp(&self, other: &Self) -> Ordering {
        if let Some(ord) = self.partial_cmp(other) {
            ord
        } else {
            self.metadata().get("Title").unwrap_or(&self.file).cmp(other.metadata().get("Title").unwrap_or(&other.file))
        }
    }
}

impl Album {
    pub fn new<S: AsRef<str>>(name: S) -> Self {
        Album {
            name: String::from(name.as_ref()),
            tracks: Vec::new()
        }
    }

    pub fn tracks(&self) -> &Vec<Track> {
        &self.tracks
    }

    pub fn tracks_mut(&mut self) -> &mut Vec<Track> {
        &mut self.tracks
    }

    pub fn name(&self) -> &String {
        &self.name
    }
}


// Albums sort alphabetically
impl Ord for Album {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialOrd for Album {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Artist {
    pub fn new<S: AsRef<str>>(name: S) -> Self {
        Artist {
            name: String::from(name.as_ref()),
            albums: Vec::new()
        }
    }

    pub fn albums(&self) -> &Vec<Album> {
        &self.albums
    }

    pub fn albums_mut(&mut self) -> &mut Vec<Album> {
        &mut self.albums
    }

    pub fn name(&self) -> &String {
        &self.name
    }
}


// Artists sort alphabetically
impl Ord for Artist {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialOrd for Artist {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Playlist {
    pub fn new<S: AsRef<str>>(name: S) -> Self {
        Playlist {
            name: String::from(name.as_ref()),
            tracks: Vec::new()
        }
    }

    pub fn tracks(&self) -> &Vec<Track> {
        &self.tracks
    }

    pub fn tracks_mut(&mut self) -> &mut Vec<Track> {
        &mut self.tracks
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn add_track(&mut self, track: &Track) {
        self.tracks.push(track.clone());
    }

    pub fn remove_track(&mut self, track: &Track) -> bool {
        if let Some(n) = self.tracks.iter().position(|t| *t == *track) {
            self.tracks.remove(n);
            true
        } else {
            false
        }
    }
}

impl LibraryData {
    pub fn new() -> Self {
        LibraryData {
            albums: Vec::new(),
            artists: Vec::new(),
            tracks: Vec::new(),
            playlists: Vec::new()
        }
    }
}

impl <'a> Library<'a> {

    pub fn new() -> Self {
        Library {
            db_path: Path::new(""),
            data: LibraryData::new()
        }
    }

    pub fn from<P: AsRef<Path>>(library_file: &'a P) -> Result<Self> {
        let mut lib = Self::new();
        lib.load(library_file)?;
        Ok(lib)
    }

    pub fn data(&self) -> LibraryData {
        self.data.clone()
    }

    pub fn artists(&self) -> &Vec<Artist> {
        &self.data.artists
    }

    pub fn artists_mut(&mut self) -> &mut Vec<Artist> {
        &mut self.data.artists
    }

    pub fn albums(&self) -> &Vec<Album> {
        &self.data.albums
    }

    pub fn albums_mut(&mut self) -> &mut Vec<Album> {
        &mut self.data.albums
    }

    pub fn tracks(&self) -> &Vec<Track> {
        &self.data.tracks
    }

    pub fn tracks_mut(&mut self) -> &mut Vec<Track> {
        &mut self.data.tracks
    }
    
    pub fn playlists(&self) -> &Vec<Playlist> {
        &self.data.playlists
    }

    pub fn playlists_mut(&mut self) -> &mut Vec<Playlist> {
        &mut self.data.playlists
    }

    /// Load the library file from file_path
    pub fn load<P: AsRef<Path>>(&mut self, library_file_path: &'a P) -> Result<()> {
        // Create the Library from the file,
        // returning if the parser encounters an error.
        let library_file = match File::open(&library_file_path) {
            Ok(f) => f,
            Err(_) => create_file(&library_file_path)?
        };
        self.data = serde_json::from_reader(library_file)?;
        self.db_path = library_file_path.as_ref();

        Ok(())
    }

    /// Add a directory containing audio files to this Library and write
    /// the resulting json to the library file on disk
    pub fn add_path<P: AsRef<Path> + Display>(&mut self, directory: P) -> io::Result<()> {
        let dir = directory.as_ref();
        let mut albums = HashMap::<String, Album>::new();
        let mut artists = HashMap::<String, Artist>::new();
        if dir.is_dir() {
            for entry in fs::read_dir(dir)? {
                let entry = entry?;
                let path = entry.path();
                self.add_path_rec(path)?;
            }   
        } else {
            return Err(io::Error::new(ErrorKind::InvalidInput, format!("{} is not a directory", directory)))
        }

        for track in self.tracks() {
            // If the track has an album in its metadata, add the album to the
            // library if it doesn't already exist, then add the track to the album
            if let Some(track_album) = track.metadata().get("Album") {
                let alb: &mut Album;
                if let Some(album) = albums.get_mut(track_album) {
                    album.tracks_mut().push(track.clone());
                    alb = album;
                } else {
                    albums.insert(track_album.clone(), Album::new(track_album));
                    alb = albums.get_mut(track_album).unwrap();
                    alb.tracks_mut().push(track.clone());
                }

                // If the track has an artist in its metadata, add the artist to the
                // library if it doesn't already exist, then add the album to the artist
                if let Some(track_artist) = track.metadata().get("Artist") {
                    if let Some(artist) = artists.get_mut(track_artist) {
                        artist.albums_mut().push(alb.clone());
                    } else {
                        artists.insert(track_artist.clone(), Artist::new(track_artist));
                        artists.get_mut(track_artist).unwrap().albums_mut().push(alb.clone());
                    }
                } else { // If the track doesn't have an artist in its metadata, add album to the (unknown) artist
                    if let Some(unknown) = artists.get_mut("(unknown)") {
                        unknown.albums_mut().push(alb.clone());
                    } else {
                        artists.insert("(unknown)".to_string(), Artist::new("(unknown)"));
                        artists.get_mut("(unknown)").unwrap().albums_mut().push(alb.clone());
                    }
                }
            } else { // If the track doesn't have an album in its metadata add track to the (unknown) album
                let ualb: &mut Album;
                if let Some(unknown_alb) = albums.get_mut("(unknown)") {
                    unknown_alb.tracks_mut().push(track.clone());
                    ualb = unknown_alb;
                } else {
                    albums.insert("(unknown)".to_string(), Album::new("(unknown)"));
                    ualb = albums.get_mut("(unknown)").unwrap();
                    ualb.tracks_mut().push(track.clone());
                }

                // If the track has an artist in its metadata, add the artist to the
                // library if it doesn't already exist, then add the album to the artist
                if let Some(track_artist) = track.clone().metadata().get("Artist") {
                    if let Some(artist) = artists.get_mut(track_artist) {
                        artist.albums_mut().push(ualb.clone());
                    } else {
                        artists.insert(track_artist.clone(), Artist::new(track_artist));
                        artists.get_mut(track_artist).unwrap().albums_mut().push(ualb.clone());
                    }
                } else { // If the track doesn't have an artist in its metadata, add album to the (unknown) artist
                    if let Some(unknown) = artists.get_mut("(unknown)") {
                        unknown.albums_mut().push(ualb.clone());
                    } else {
                        artists.insert("(unknown)".to_string(), Artist::new("(unknown)"));
                        artists.get_mut("(unknown)").unwrap().albums_mut().push(ualb.clone());
                    }
                }
            }
        }

        self.data.albums = {
            let mut v = Vec::new();
            for a in albums.values() {
                v.push(a.clone());
            }
            v.sort();
            v
        };

        for a in self.data.albums.iter_mut() {
            a.tracks_mut().sort();
        }

        self.data.artists = {
            let mut v = Vec::new();
            for a in artists.values() {
                v.push(a.clone());
            }
            v.sort();
            v
        };

        for a in self.data.artists.iter_mut() {
            a.albums_mut().sort();
        }

        self.data.tracks.sort();

        // Convert updated library to json and write to file
        let json = serde_json::to_string(&self.data)?;
        fs::write(self.db_path, json)
    }

    fn add_path_rec<P: AsRef<Path>>(&mut self, directory: P) -> io::Result<()> {
        let path = directory.as_ref();
        // Recurse through directories until a non-directory file is reached
        if path.is_dir() {
            for entry in fs::read_dir(path)? {
                let entry = entry?;
                let p = entry.path();
                self.add_path_rec(p)?;
            }
        } else {
            // Create new track from file and add it to the library
            let track;
            let file = path.to_str().unwrap().to_string();
            if let Ok(metadata) = metadata::get_metadata(&file) {
                track = Track {
                    file,
                    metadata
                };
                self.data.tracks.push(track.clone());
            }
        }

        Ok(())
    }
}

fn create_dir<P: AsRef<Path>>(path: &P) -> Result<(), io::Error> {
    match fs::create_dir(path) {
        Ok(_) => Ok(()),
        Err(e) => {
            if e.kind() == ErrorKind::NotFound {
                create_dir(&path.as_ref().parent().unwrap())?;
                Ok(())
            } else {
                Err(e)
            }
        }
    }
}

fn create_file<P: AsRef<Path>>(path: &P) -> Result<File> {
    match File::create(path) {
        Ok(f) => Ok(f),
        Err(_) => {
            create_dir(&path.as_ref().parent().unwrap())?;
            Ok(File::create(path)?)
        }
    }
}