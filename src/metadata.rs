/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{
    collections::HashMap,
    fs::File,
    io::BufReader,
    path::Path
};

use anyhow::{Result, Error};
use id3::{
    Tag as ID3Tag, 
    frame::Content
};
use lewton::inside_ogg::read_headers;
use metaflac::{
    Tag as FLACTag,
    block::{
        Block,
        VorbisComment
    }
};
use ogg::reading::PacketReader;

/// Get the metadata from an audio file and convert it to a HashMap<String, String>.
/// Returns an error if the file is not a supported audio file type.
/// Currently supports id3 (mp3, mp2, mp1) and flac tags.
pub fn get_metadata<P: AsRef<Path>>(path: &P) -> Result<HashMap<String, String>> {
    let mut md = HashMap::<String, String>::new();
    let file = path.as_ref();
    if let Some(ext) = file.extension() {
        match ext.to_str() {
            Some("mp3") | Some("mp2") | Some("mp1") => {
                let tags = ID3Tag::read_from_path(file)?;
                for t in tags.frames() {
                    if id3_content_to_string(t.content()) != String::new() {
                        let id = match t.id() {
                            "TIT2" => "Title".to_string(),
                            "TALB" => "Album".to_string(),
                            "TPE1" => "Artist".to_string(),
                            "TRCK" => "Track".to_string(),
                            _ => t.id().to_string()
                        };
                        let content = if id == "Track".to_string() {
                            let mut s = String::new();
                            for c in id3_content_to_string(t.content()).chars() {
                                if c != '/' {
                                    s.push(c);
                                } else {
                                    break;
                                }
                            }
                            s
                        } else {
                            id3_content_to_string(t.content())
                        };
                        md.insert(id, content);
                    }
                }
            },
            Some("flac") => {
                let tags = FLACTag::read_from_path(file)?;
                for t in tags.blocks() {
                    if let Block::VorbisComment(vc) = t {
                        add_flac_vc_to_map(&mut md, vc);
                    }
                }
            },
            Some("ogg") => {
                let mut reader = PacketReader::new(BufReader::new(File::open(file)?));
                let (headers, _n) = read_headers(&mut reader)?;
                let comments = headers.1.comment_list;
                add_ogg_vcs_to_map(&mut md, &comments);
            }
            _ => { return Err(Error::msg("Filetype not recognized")); }
        }
    }

    Ok(md)
}

fn id3_content_to_string(content: &Content) -> String {
    match content {
        Content::Text(s) => s.clone(),
        Content::ExtendedText(et) => {
            let mut s = et.description.clone();
            s.push('\n');
            s.push_str(et.value.as_str());
            s
        },
        Content::Link(s) => s.clone(),
        Content::ExtendedLink(el) => {
            let mut s = el.description.clone();
            s.push('\n');
            s.push_str(el.link.as_str());
            s
        },
        Content::Comment(c) => {
            let mut s = c.lang.clone();
            s.push('\n');
            s.push_str(c.description.as_str());
            s.push('\n');
            s.push_str(c.text.as_str());
            s
        },
        Content::Unknown(v) => {
            String::from_utf8_lossy(v).to_string()
        }

        // Ignoring Lyrics, SynchronizedLyrics, Picture, and EncapsulatedObject because
        // they're not necessary here.
        _ => String::new()
    }
}

fn add_flac_vc_to_map(map: &mut HashMap<String, String>, vc: &VorbisComment) {
    if let Some(artists) = vc.artist() {
        if let Some(artist) = artists.get(0) {
            map.insert("Artist".to_string(), artist.clone());
        }
    }
    if let Some(albums) = vc.album() {
        if let Some(album) = albums.get(0) {
            map.insert("Album".to_string(), album.clone());
        }
    }
    if let Some(track_num) = vc.track() {
        map.insert("Track".to_string(), track_num.to_string());
    }
    if let Some(titles) = vc.title() {
        if let Some(title) = titles.get(0) {
            map.insert("Title".to_string(), title.to_string());
        }
    }
}

fn add_ogg_vcs_to_map(map: &mut HashMap<String, String>, vc: &Vec<(String, String)>) {
    for (key, val) in vc.iter() {
        let mut k = key.clone();
        k.make_ascii_lowercase();
        match k.as_str() {
            "artist" => {
                map.insert("Artist".to_string(), val.clone());
            },
            "album" => {
                map.insert("Album".to_string(), val.clone());
            },
            "title" => {
                map.insert("Title".to_string(), val.clone());
            },
            "tracknumber" => {
                map.insert("Track".to_string(), val.clone());
            },
            _ => ()
        }
    }
}