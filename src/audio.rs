/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{
    fs::File,
    io::BufReader
};

use anyhow::Result;
use rodio::{
    Decoder, 
    OutputStream, 
    OutputStreamHandle, 
    Sink
};

use crate::music_library::Track;

pub struct AudioSystem {
    sink: Sink,
    stream_handle: OutputStreamHandle,
    _stream: OutputStream
}

impl AudioSystem {
    /// Attempts to create a new AudioSystem
    pub fn try_new() -> Result<Self> {
        let (stream, stream_handle) = OutputStream::try_default()?;
        let sink = Sink::try_new(&stream_handle)?;
        Ok(AudioSystem {
            sink,
            stream_handle,
            _stream: stream
        })
    }

    /// Stops the currently playing Track and plays the given Track
    pub fn play_track(&mut self, track: &Track) -> Result<()> {
        let file = BufReader::new(File::open(track.path())?);
        let source = Decoder::new(file)?;
        if !self.sink.empty() {
            self.sink.stop();
            self.sink = Sink::try_new(&self.stream_handle)?;
        }
        self.sink.append(source);
        Ok(())
    }

    pub fn pause(&self) {
        self.sink.pause();
    }

    pub fn resume(&self) {
        self.sink.play();
    }

    pub fn is_paused(&self) -> bool {
        self.sink.is_paused()
    }
}