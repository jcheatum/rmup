/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{
    env,
    io,
    fs,
    process
};

use anyhow::{
    Result
};
use crossterm::terminal::LeaveAlternateScreen;
use getopts::Options;

mod audio;
mod metadata;
mod player;
pub mod music_library;

use music_library::Library;

fn main() -> Result<()> {
    let argv: Vec<String> = env::args().collect();
    let prog = &argv[0];
    let mut opts = Options::new();
    opts.optopt("c", "config", "Specify config file location", "FILE");
    opts.optopt("a", "add", "Add a directory to library", "DIR");
    opts.optopt("l", "lib", "Use the given library file", "FILE");
    opts.optflag("h", "help", "print usage and exit");
    let matches = match opts.parse(&argv[1..]) {
        Ok(m) => m,
        Err(f) => {
            eprintln!("{}: Error: {}", prog, f);
            process::exit(1);
        }
    };
    if matches.opt_present("h") {
        print_usage(prog, opts);
        process::exit(0);
    }

    let library_file_path = matches.opt_get_default(
        "l",
        if cfg!(windows) {
            format!("{}{}", dirs_next::data_dir().unwrap().to_str().unwrap(), "\\rmup\\library.json")
        } else {
            format!("{}{}", dirs_next::data_dir().unwrap().to_str().unwrap(), "/rmup/library.json")
        }
    ).unwrap();

    let mut library = match Library::from(&library_file_path) {
        Ok(l) => l,
        Err(_) => {
            let l = Library::new();
            fs::write(&library_file_path, serde_json::to_string(&l.data())?)?;
            l
        }
    };

    if let Ok(Some(dir)) = matches.opt_get::<String>("a") {
        library.add_path(dir)?;
    }

    match player::run(&library) {
        Ok(ok) => Ok(ok),
        Err(e) => {
            crossterm::terminal::disable_raw_mode();
            crossterm::execute!(io::stdout(), LeaveAlternateScreen)?;
            Err(e)
        }
    }
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}